import React, {useState} from 'react';
import {Button, Grid, IconButton, makeStyles} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

import FormElement from "../UI/Form/FormElement";
import FileInput from "../UI/Form/FileInput";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";


const initIngredientObject =  {title: "", amount: ""};

const useStyles = makeStyles({
  ingredientItem: {
    padding: "10px 65px 10px 10px",
    position: "relative",
  },
  button: {
    position: "absolute",
    top: "50%",
    right: "10px",
    transform: "translateY(-50%)",
  },
  pr: {
    paddingRight: "10px"
  }
});

const CocktailForm = ({onSubmit, error, loading}) => {
  const classes = useStyles();
  const [newCocktail, setNewCocktail] = useState({
    name: "",
    recipe: "",
    image: "",
  });
  const [ingredients, setIngredients] = useState([initIngredientObject]);

  const cocktailInputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;

    setNewCocktail(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const cocktailImageChangeHandler = e => {
    const name = e.target.name;
    const file = e.target.files[0];

    setNewCocktail(prevState => ({
      ...prevState,
      [name]: file
    }));
  };

  const addIngredientField = () => {
    setIngredients(prevState => ([...prevState, initIngredientObject]));
  };

  const changeIngredient= (i, name, value) => {
    setIngredients(prevState => {
      const ingFieldCopy = {
        ...prevState[i],
        [name]: value,
      };

      return prevState.map((ingredient, index) => {
        if (index === i) {
          return ingFieldCopy;
        }

        return ingredient
      });
    });
  };

  const submitFormHandler = e => {
    e.preventDefault();

    const formData = new FormData();

    const cocktailData = {...newCocktail, ingredients};

    Object.keys(cocktailData).forEach(key => {
      if (key === "ingredients") {
        formData.append(key, JSON.stringify(cocktailData[key]));
      } else {
        formData.append(key, cocktailData[key]);
      }
    });

    onSubmit(formData);
  };

  const removeIngredientField = index => {
    setIngredients(prevState => {
      const ingredientsStateCopy = [...prevState];
      ingredientsStateCopy.splice(index, 1);

      return ingredientsStateCopy;
    });
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  const ingredientsBlock = ingredients.map((ingredientItem, index) => {
    return (
        <Grid item container key={index} alignItems="center" className={classes.ingredientItem}>
          <Grid item xs className={classes.pr}>
            <FormElement
                label="Ingredient name"
                name="title"
                type="text"
                onChange={event => changeIngredient(index, event.target.name, event.target.value)}
                value={ingredients[index].title}
            />
          </Grid>
          <Grid item xs={4} className={classes.pr}>
            <FormElement
                label="Amount"
                name="amount"
                type="text"
                onChange={event => changeIngredient(index, event.target.name, event.target.value)}
                value={ingredients[index].amount}
            />
          </Grid>

          <Grid item>
            <IconButton
                // className={classes.button}
                onClick={() => removeIngredientField(index)}
            >
              <DeleteIcon color="secondary"/>
            </IconButton>
          </Grid>
        </Grid>
    );
  });

  return (
      <Grid
          container
          spacing={1}
          direction="column"
          component="form"
          onSubmit={submitFormHandler}
          noValidate
      >
        <FormElement
            required
            label="Name"
            name="name"
            type="text"
            onChange={cocktailInputChangeHandler}
            value={newCocktail.name}
            error={getFieldError("name")}
        />
        {ingredientsBlock}
        <Grid item>
          <Button
              variant="contained"
              color="primary"
              onClick={addIngredientField}
          >
            Add ingredient
          </Button>
        </Grid>
        <FormElement
            required
            multiline
            rows={4}
            label="Recipe"
            name="recipe"
            type="text"
            onChange={cocktailInputChangeHandler}
            value={newCocktail.recipe}
            error={getFieldError("recipe")}
        />
        <Grid item xs>
          <FileInput
              label="Image"
              name="image"
              onChange={cocktailImageChangeHandler}
              error={getFieldError("image")}
          />
        </Grid>
        <Grid item>
          <ButtonWithProgress
              type="submit"
              variant="contained"
              color="primary"
              loading={loading}
              disabled={loading}
          >
            Add cocktail
          </ButtonWithProgress>
        </Grid>
      </Grid>
  );
};

export default CocktailForm;