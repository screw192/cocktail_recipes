import React from 'react';
import {CircularProgress, makeStyles} from "@material-ui/core";


const useStyles = makeStyles({
  preloaderBox: {
    height: "300px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  }
});

const Preloader = () => {
  const classes = useStyles();

  return (
      <div className={classes.preloaderBox}>
        <CircularProgress />
      </div>
  );
};

export default Preloader;