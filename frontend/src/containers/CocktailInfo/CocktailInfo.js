import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {
  Box,
  CardMedia,
  Container,
  Grid,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Paper,
  Typography
} from "@material-ui/core";
import RemoveIcon from '@material-ui/icons/Remove';

import {fetchCocktailDetailsRequest} from "../../store/actions/cocktailsActions";
import {apiURL} from "../../config";
import Preloader from "../../components/UI/Preloader/Preloader";
import {Helmet} from "react-helmet";


const useStyles = makeStyles({
  cocktailImage: {
    height: 300,
    borderRadius: 5,
  },
  listIcon: {
    minWidth: 35,
  }
});

const CocktailInfo = props => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const cocktailDataLoading = useSelector(state => state.cocktails.cocktailDetailsLoading);
  const cocktailData = useSelector(state => state.cocktails.cocktailDetails);
  const cocktailID = props.match.params.id;

  useEffect(() => {
    dispatch(fetchCocktailDetailsRequest(cocktailID));
  }, [dispatch, cocktailID]);

  const ratingCounter = () => {
    const ratingSum = cocktailData?.ratings.reduce((accumulator, currentValue) => {
      return accumulator + currentValue.rating;
    }, 0);

    return Math.floor(ratingSum / cocktailData?.ratings.length);
  };

  const ingredientsList = cocktailData?.ingredients.map(ingredientItem => {
    return (
        <ListItem key={ingredientItem._id}>
          <ListItemIcon className={classes.listIcon}>
            <RemoveIcon/>
          </ListItemIcon>
          <ListItemText
              primary={`${ingredientItem.amount} ${ingredientItem.title}`}
          />
        </ListItem>
    );
  });

  return (
      cocktailDataLoading ? (
          <Preloader/>
      ) : (
          <Container maxWidth="md">
            <Helmet>
              <title>{cocktailData.name}</title>
            </Helmet>
            <Box p={3} my={4} component={Paper} elevation={5}>
              <Typography variant="h4" paragraph>{cocktailData.name}</Typography>
              <Grid container spacing={4}>
                <Grid item xs={4}>
                  <CardMedia
                      className={classes.cocktailImage}
                      image={`${apiURL}/${cocktailData.image}`}
                  />
                </Grid>
                <Grid item xs>
                  {cocktailData.published ? (
                      <>
                        <Typography variant="h6" gutterBottom>
                          Rating: {ratingCounter()} ({cocktailData.ratings.length} votes)
                        </Typography>
                      </>
                  ) : (
                      <Typography variant="h6" color="textSecondary">
                        Rating not available
                      </Typography>
                  )}
                  <Typography variant="h6">
                    Ingredients:
                  </Typography>
                  <List>
                    {ingredientsList}
                  </List>
                </Grid>
              </Grid>
              <Typography variant="h6">
                Recipe:
              </Typography>
              <Typography variant="body1">
                {cocktailData.recipe}
              </Typography>
            </Box>
          </Container>
      )
  );
};

export default CocktailInfo;