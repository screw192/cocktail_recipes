import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Box, Grid, Typography} from "@material-ui/core";
import AddCircleIcon from '@material-ui/icons/AddCircle';

import {fetchCocktailsRequest} from "../../store/actions/cocktailsActions";
import CocktailCard from "./CocktailCard";
import Button from "@material-ui/core/Button";
import {Link} from "react-router-dom";
import {Helmet} from "react-helmet";
import Preloader from "../../components/UI/Preloader/Preloader";


const Cocktails = props => {
  const dispatch = useDispatch();
  const cocktailsData = useSelector(state => state.cocktails.cocktails);
  const cocktailsLoading = useSelector(state => state.cocktails.cocktailsLoading);
  const user = useSelector(state => state.users.user);

  useEffect(() => {
    if (props.match.params.user) {
      dispatch(fetchCocktailsRequest(`user_cocktails/${props.match.params.user}`));
    } else {
      dispatch(fetchCocktailsRequest("all"));
    }
  }, [dispatch, user, props]);

  const cocktailCards = cocktailsData.map(cocktailItem => {
    return (
        <CocktailCard
            key={cocktailItem._id}
            id={cocktailItem._id}
            name={cocktailItem.name}
            image={cocktailItem.image}
            published={cocktailItem.published}
            role={user?.role}
        />
    );
  });

  return (
      <>
        <Helmet>
          <title>{props.match.params.user && "My cocktails"}</title>
        </Helmet>
        <Box my={4} component={Grid} container justify="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5">
              {props.match.params.user ? "My cocktails" : "Cocktails"}
            </Typography>
          </Grid>
          <Grid item>
            <Button
                variant="contained"
                color="primary"
                endIcon={<AddCircleIcon/>}
                component={Link}
                to="/add_cocktail"
            >
              Add
            </Button>
          </Grid>
        </Box>
        {cocktailsLoading ? (
            <Preloader/>
        ) : (
            <Grid container spacing={4}>
              {cocktailCards}
            </Grid>
        )}
      </>

  );
};

export default Cocktails;