import React from 'react';
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  IconButton,
  makeStyles,
  Typography
} from "@material-ui/core";
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import {apiURL} from "../../config";
import {Link} from "react-router-dom";
import {useDispatch} from "react-redux";
import {publishCocktailRequest, removeCocktailRequest} from "../../store/actions/cocktailsActions";


const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    position: "relative",
  },
  media: {
    height: 200,
  },
  publishButton: {
    color: "mediumseagreen"
  },
  removeButton: {
    padding: "3px",
    backgroundColor: "#ffffff99",
    position: "absolute",
    top: "5px",
    right: "5px",
    "&:hover": {
      backgroundColor: "#fff",
    }
  }
});

const CocktailCard = ({role, id, name, image, published = true}) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const publishCocktailHandler = () => {
    dispatch(publishCocktailRequest(id));
  };

  const removeCocktailHandler = event => {
    event.preventDefault();
    dispatch(removeCocktailRequest(id));
  };

  return (
      <Grid item xs={6} sm={4} md={3}>
        <Card className={classes.root}>
          <CardMedia
              className={classes.media}
              image={`${apiURL}/${image}`}
          />
          <CardContent>
            <Typography variant="h6" component="h2" noWrap>
              {name}
            </Typography>
          </CardContent>
          <CardActions>
            <Grid container justify="space-between">
              <Grid item>
                <Button size="small" color="primary" component={Link} to={`/cocktails/${id}`}>
                  More...
                </Button>
              </Grid>
              <Grid item>
                {role === "admin" ? !published && (
                    <Button size="small" className={classes.publishButton} onClick={publishCocktailHandler}>
                      Publish
                    </Button>
                ) : !published && (
                    <Button size="small" color="primary" disabled>
                      Unpublished
                    </Button>
                )}
              </Grid>
            </Grid>
          </CardActions>
          {role === "admin" && (
              <IconButton className={classes.removeButton} color="secondary" onClick={e => removeCocktailHandler(e)}>
                <DeleteForeverIcon/>
              </IconButton>
          )}
        </Card>
      </Grid>
  );
};

export default CocktailCard;