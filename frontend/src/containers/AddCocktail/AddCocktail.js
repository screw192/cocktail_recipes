import React from 'react';
import {Container, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";

import CocktailForm from "../../components/CocktailForm/CocktailForm";
import {createCocktailRequest} from "../../store/actions/cocktailsActions";
import {Helmet} from "react-helmet";


const AddCocktail = () => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.cocktails.createCocktailLoading);
  const error = useSelector(state => state.cocktails.createCocktailError);

  const cocktailFormSubmit = cocktailData => {
    dispatch(createCocktailRequest(cocktailData));
  };

  return (
      <Container maxWidth="md">
        <Helmet>
          <title>Add you cock</title>
        </Helmet>
        <Typography variant="h5" gutterBottom>
          Add your cocktail recipe
        </Typography>
        <CocktailForm
            onSubmit={cocktailFormSubmit}
            loading={loading}
            error={error}
        />
      </Container>
  );
};

export default AddCocktail;