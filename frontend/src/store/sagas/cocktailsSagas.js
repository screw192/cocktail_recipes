import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {addNotification} from "../actions/notifierActions";
import {
  createCocktailFailure, createCocktailRequest,
  createCocktailSuccess,
  fetchCocktailDetailsFailure, fetchCocktailDetailsRequest, fetchCocktailDetailsSuccess,
  fetchCocktailsFailure,
  fetchCocktailsRequest,
  fetchCocktailsSuccess, publishCocktailRequest, removeCocktailRequest
} from "../actions/cocktailsActions";
import {historyPush} from "../actions/historyActions";

export function* fetchCocktails({payload: urlEnd}) {
  try {
    const response = yield axiosApi.get(`/cocktails/${urlEnd}`);
    yield put(fetchCocktailsSuccess(response.data));
  } catch (e) {
    yield put(fetchCocktailsFailure(e.response.data));
    yield put(addNotification({message: "Failed to fetch cocktails", options: {variant: 'error'}}));
  }
}

export function* fetchCocktailDetails({payload: cocktailID}) {
  try {
    const response = yield axiosApi.get(`/cocktails/${cocktailID}`);
    yield put(fetchCocktailDetailsSuccess(response.data));
  } catch (e) {
    yield put(fetchCocktailDetailsFailure(e.response.data));
    yield put(addNotification({message: "Failed to fetch cocktail", options: {variant: 'error'}}));
  }
}

export function* createCocktail({payload: cocktailData}) {
  try {
    yield axiosApi.post("/cocktails", cocktailData);
    yield put(createCocktailSuccess());
    yield put(historyPush("/"));
    yield put(addNotification({message: "Your cocktail is now in pre-moderation stage.", options: {variant: "info"}}));
  } catch (e) {
    yield put(createCocktailFailure(e));
    yield put(addNotification({message: "Failed to create cocktail", options: {variant: "error"}}));
  }
}

export function* publishCocktail({payload: cocktailID}) {
  try {
    yield axiosApi.patch(`/cocktails/${cocktailID}`);
    yield put(historyPush("/"));
    yield put(addNotification({message: "Cocktail has been published", options: {variant: "success"}}));
  } catch (e) {
    yield put(addNotification({message: "Failed to publish cocktail", options: {variant: "error"}}));
  }
}

export function* removeCocktail({payload: cocktailID}) {
  try {
    yield axiosApi.delete(`/cocktails/${cocktailID}`);
    yield put(historyPush("/"));
    yield put(addNotification({message: "Cocktail successfully deleted", options: {variant: "success"}}));
  } catch (e) {
    yield put(addNotification({message: "Failed to delete cocktail", options: {variant: "error"}}));
  }
}

const cocktailsSagas = [
    takeEvery(fetchCocktailsRequest, fetchCocktails),
    takeEvery(fetchCocktailDetailsRequest, fetchCocktailDetails),
    takeEvery(createCocktailRequest, createCocktail),
    takeEvery(publishCocktailRequest, publishCocktail),
    takeEvery(removeCocktailRequest, removeCocktail),
];

export default cocktailsSagas;