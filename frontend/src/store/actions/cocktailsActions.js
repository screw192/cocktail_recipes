import cocktailsSlice from "../slices/cocktailsSlice";

export const {
  fetchCocktailsRequest,
  fetchCocktailsSuccess,
  fetchCocktailsFailure,
  fetchCocktailDetailsRequest,
  fetchCocktailDetailsSuccess,
  fetchCocktailDetailsFailure,
  createCocktailRequest,
  createCocktailSuccess,
  createCocktailFailure,
  publishCocktailRequest,
  removeCocktailRequest,
} = cocktailsSlice.actions;