import {createSlice} from "@reduxjs/toolkit";

export const initialState = {
  cocktails: [],
  cocktailsLoading: true,
  cocktailsError: null,
  cocktailDetails: null,
  cocktailDetailsLoading: true,
  cocktailDetailsError: null,
  createCocktailLoading: false,
  createCocktailError: null
};

const name = "cocktails";

const cocktailsSlice = createSlice({
  name,
  initialState,
  reducers: {
    fetchCocktailsRequest: state => {
      state.cocktailsLoading = true;
    },
    fetchCocktailsSuccess: (state, {payload: cocktailsData}) => {
      state.cocktailsLoading = false;
      state.cocktails = cocktailsData;
      state.cocktailDetails = null;
      state.cocktailDetailsLoading = true;

    },
    fetchCocktailsFailure: (state, {payload: error}) => {
      state.cocktailsLoading = false;
      state.cocktailsError = error;
    },
    fetchCocktailDetailsRequest: state => {
      state.cocktailDetailsLoading = true;
    },
    fetchCocktailDetailsSuccess: (state, {payload: cocktailDetailsData}) => {
      state.cocktailDetailsLoading = false;
      state.cocktailDetails = cocktailDetailsData;
    },
    fetchCocktailDetailsFailure: (state, {payload: error}) => {
      state.cocktailDetailsLoading = false;
      state.cocktailDetailsError = error;
    },
    createCocktailRequest: state => {
      state.createCocktailLoading = true;
    },
    createCocktailSuccess: state => {
      state.createCocktailLoading = false;
    },
    createCocktailFailure: (state, {payload: error}) => {
      state.createCocktailLoading = false;
      state.createCocktailError = error;
    },
    publishCocktailRequest: () => {},
    removeCocktailRequest: () => {},
  },
});

export default cocktailsSlice;