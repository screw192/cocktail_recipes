import React from "react";
import {Route, Switch} from "react-router-dom";
import {Helmet} from "react-helmet";

import Layout from "./components/UI/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Cocktails from "./containers/Cocktails/Cocktails";
import CocktailInfo from "./containers/CocktailInfo/CocktailInfo";
import AddCocktail from "./containers/AddCocktail/AddCocktail";


const App = () => {
  return (
    <Layout>
      <Helmet
        titleTemplate="Cocks&tails - %s"
        defaultTitle="Cocks&tails"
      />
      <Switch>
        <Route path="/" exact component={Cocktails}/>
        <Route path="/my_cocktails/:user" exact component={Cocktails}/>
        <Route path="/cocktails/:id" exact component={CocktailInfo}/>
        <Route path="/add_cocktail" exact component={AddCocktail}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
      </Switch>
    </Layout>
  );
};

export default App;
