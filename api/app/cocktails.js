const express = require("express");
const Cocktail = require("../models/Cocktail");
const upload = require("../multer").cocktails;
const roleCheck = require("../middleware/roleCheck");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get("/all", roleCheck, async (req, res) => {
  try {
    let cocktails;

    switch (req.role) {
      case "admin":
        cocktails = await Cocktail.find().select(["name", "image", "published"]);
        break;
      case "user":
        cocktails = await Cocktail.find({published: "true"}).select(["name", "image"]);
        break;
      default:
        break;
    }

    res.send(cocktails);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/user_cocktails/:id", auth, async (req, res) => {
  try {
    if (!req.user._id.equals(req.params.id)) {
      return res.status(403).send({message: "Forbidden"});
    }

    const cocktails = await Cocktail.find({user: req.params.id}).select(["name", "image", "published"]);

    res.send(cocktails);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const cocktailData = await Cocktail.findOne({_id: req.params.id});

    res.send(cocktailData);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  try {
    const cocktailData = {
      user: req.user._id,
      name: req.body.name,
      recipe: req.body.recipe,
      ingredients: JSON.parse(req.body.ingredients),
    };
    cocktailData.image = req.file.filename;

    const cocktail = new Cocktail(cocktailData);
    await cocktail.save();

    res.send(cocktail);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.patch("/:id", auth, permit("admin"), async (req, res) => {
  try {
    const cocktail = await Cocktail.findOneAndUpdate(
        {_id: req.params.id},
        {published: true},
        {new: true}
    );

    res.send(cocktail);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    await Cocktail.deleteOne({_id: req.params.id});
    res.send({message: "Cocktail successfully deleted"});
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;

