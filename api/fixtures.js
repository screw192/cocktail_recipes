const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const Cocktail = require("./models/Cocktail");
const {nanoid} = require("nanoid");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) { 
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [admin, user1, user2, user3] = await User.create({
    email: "admin@test",
    password: "test",
    token: nanoid(),
    displayName: "Abu Makaka",
    role: "admin"
  }, {
    email: "user1@test",
    password: "test",
    token: nanoid(),
    displayName: "Yoba YellowKolobok",
  }, {
    email: "user2@test",
    password: "test",
    token: nanoid(),
    displayName: "Anonim Legion"
  }, {
    email: "user3@test",
    password: "test",
    token: nanoid(),
    displayName: "Tyanocka"
  });

  await Cocktail.create({
    user: admin,
    name: "Negroni",
    image: "fixtures/negroni.png",
    recipe: "Stir into glass over ice, garnish and serve.",
    published: true,
    ingredients: [
      {title: "Gin", amount: "1 oz"},
      {title: "Campari", amount: "1 oz"},
      {title: "Sweet Vermouth", amount: "1 oz"},
    ],
    ratings: [
      {user: admin, rating: 5},
      {user: user1, rating: 2},
      {user: user2, rating: 5},
      {user: user3, rating: 1}
    ],
  }, {
    user: admin,
    name: "Whiskey Sour",
    image: "fixtures/whiskey_sour.png",
    recipe: "Shake with ice. Strain into chilled glass, garnish and serve. If served 'On the rocks', strain ingredients into old-fashioned glass filled with ice.",
    published: true,
    ingredients: [
      {title: "Blended whiskey", amount: "2 oz"},
      {title: "Lemon", amount: "Juice of 1/2"},
      {title: "Powdered sugar", amount: "1/2 tsp"},
      {title: "Cherry", amount: "1"},
      {title: "Lemon", amount: "1/2 slice"},
    ],
    ratings: [
      {user: admin, rating: 5},
      {user: user1, rating: 4},
      {user: user2, rating: 1}
    ],
  }, {
    user: user1,
    name: "Rum Screwdriver",
    image: "fixtures/rum_screwdriver.png",
    recipe: "Pour rum into a highball glass over ice cubes. Add orange juice, stir, and serve.",
    published: false,
    ingredients: [
      {title: "Light rum", amount: "1 1/2 oz"},
      {title: "Orange juice", amount: "5 oz"},
    ],
    ratings: [],
  }, {
    user: user2,
    name: "Snowday",
    image: "fixtures/snowday.png",
    recipe: "Stir all ingredients with ice. Strain into a chilled rocks glass over fresh ice. Express orange peel over drink and garnish.",
    published: true,
    ingredients: [
      {title: "Vodka", amount: "1 oz"},
      {title: "Amaro Montenegro", amount: "1 oz"},
      {title: "Ruby Port", amount: "1 oz"},
      {title: "Blood Orange", amount: "1 tsp"},
      {title: "Angostura Bitters", amount: "Dash"},
      {title: "Orange Peel", amount: "Garnish with"},
    ],
    ratings: [
      {user: admin, rating: 3},
      {user: user1, rating: 5}
    ],
  }, {
    user: user3,
    name: "Pink Moon",
    image: "fixtures/pink_moon.png",
    recipe: "Slowly shake in a shaker with ice, strain into a square whiskey glass. Top with fresh ice. Add the blackberries to garnish. Add flowers and a green leaf for a special look!",
    published: false,
    ingredients: [
      {title: "Gin", amount: "1 shot"},
      {title: "Coconut Liqueur", amount: "1 shot"},
      {title: "Elderflower cordial", amount: "25 ml"},
      {title: "Lime Juice", amount: "30 ml"},
      {title: "Blackberries", amount: "Garnish with"},
    ],
    ratings: [],
  });

  await mongoose.connection.close();
};

run().catch(console.error);