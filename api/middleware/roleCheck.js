const User = require("../models/User");

const roleCheck = async (req, res, next) => {
  const token = req.get('Authorization');
  const user = await User.findOne({token});

  switch (user?.role) {
    case "admin":
      req.role = "admin";
      break;
    case "user":
      req.role = "user";
      break;
    default:
      req.role = "user";
      break;
  }

  next();
};

module.exports = roleCheck;