const mongoose = require("mongoose");

const IngredientSchema = new mongoose.Schema({
  title: String,
  amount: String,
});

const RatingSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  rating: {
    type: Number,
    min: 1,
    max: 5,
  }
});

const CocktailSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  recipe: {
    type: String,
    required: true,
  },
  published: {
    type: Boolean,
    default: false,
  },
  ingredients: [IngredientSchema],
  ratings: [RatingSchema],
});

const Cocktail = mongoose.model("Cocktail", CocktailSchema);
module.exports = Cocktail;